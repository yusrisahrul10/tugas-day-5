import React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import TextField from "@mui/material/TextField";
import { Grid, styled } from "@mui/material";
import { green } from "@mui/material/colors";
import { NavLink } from "react-router-dom/cjs/react-router-dom";

export const DialogUser = ({
  opendialog,
  SetOpenDialog,
  nama,
  SetNamaUser,
  alamat,
  SetAlamatUser,
  hobi,
  SetHobiUser,
  validate,
}) => {
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(green[700]),
    backgroundColor: green[700],
    "&:hover": {
      backgroundColor: green[990],
    },
    alignItems: "center",
  }));

  return (
    <Modal open={opendialog} onClose={() => SetOpenDialog(false)}>
      <Box sx={style}>
        <Typography variant="h4" component={"h1"} textAlign={"center"}>
          Add User
        </Typography>
        <form>
          <Typography variant="h6" component={"h1"}>
            Name
          </Typography>
          <TextField
            id="outlined-basic"
            label="Name"
            variant="outlined"
            fullWidth
            value={nama}
            onChange={(e) => {
              SetNamaUser(e.target.value);
            }}
          />
          <Typography variant="h6" component={"h1"}>
            Address
          </Typography>
          <TextField
            id="outlined-basic"
            label="Address"
            variant="outlined"
            fullWidth
            value={alamat}
            onChange={(e) => {
              SetAlamatUser(e.target.value);
            }}
          />
          <Typography variant="h6" component={"h1"}>
            Hobby
          </Typography>
          <TextField
            id="outlined-basic"
            label="Hobby"
            variant="outlined"
            fullWidth
            value={hobi}
            onChange={(e) => {
              SetHobiUser(e.target.value);
            }}
          />
          <Grid container justifyContent="center">
            <ColorButton
              variant="contained"
              sx={{ textTransform: "none", mt: 3 }}
              onClick={validate}
            >
              Save
              {/* <NavLink to="/" className="nav-link">
                Save
              </NavLink> */}
            </ColorButton>
          </Grid>
        </form>
      </Box>
    </Modal>
  );
};
