import React from "react";
import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import { green } from "@mui/material/colors";
import { styled } from "@mui/material";
import { NavLink } from "react-router-dom/cjs/react-router-dom";
import "../css/Header.css";

const ColorButton = styled(Button)(({ theme }) => ({
  color: theme.palette.getContrastText(green[500]),
  backgroundColor: green[500],
  "&:hover": {
    backgroundColor: green[700],
  },
}));

export const Header = ({ OpenModal, SaveUser, ArrayUser }) => {
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div">
            <NavLink
              exact
              to="/"
              activeClassName="active-link nav-link"
              className="nav-link"
            >
              My App
            </NavLink>
          </Typography>
          <Typography variant="h6" component="div" sx={{ ml: 2 }}>
            <NavLink
              to="/about"
              activeClassName="active-link nav-link"
              className="nav-link"
            >
              About
            </NavLink>
          </Typography>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1, ml: 2 }}>
            <NavLink
              to={{
                pathname: "/hobby",
                state: { myArray: ArrayUser },
              }}
              activeClassName="active-link nav-link"
              className="nav-link"
            >
              Hobby
            </NavLink>
          </Typography>
          <ColorButton
            variant="contained"
            sx={{ textTransform: "none", ml: 6 }}
            onClick={() => {
              OpenModal();
              SaveUser();
            }}
          >
            Add User
          </ColorButton>
        </Toolbar>
      </AppBar>
    </Box>
  );
};
