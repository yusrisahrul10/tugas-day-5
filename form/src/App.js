import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import { Form } from "./page/Form";
import { Hobby } from "./page/Hobby";
import { About } from "./page/About";
import { View } from "./page/View";
import { Header } from "./assets/component/Header";

export const App = () => {
  const [open, setOpen] = useState(false);
  const [nama, setNama] = useState("");
  const [alamat, setAlamat] = useState("");
  const [hobi, setHobi] = useState("");
  const [IsEdit, setIsEdit] = useState(false);
  const [save, setSave] = useState(false);
  const [ArrayUser, setArrayUser] = useState([]);

  const [OpenAlert, setOpenAlert] = useState(false);
  const [IdData, setIdData] = useState(0);
  const [Submit, setSubmit] = useState(false);
  const [Index, setIndex] = useState();

  const validate = () => {
    if (nama.length !== 0 && alamat.length !== 0 && hobi.length !== 0) {
      if (!IsEdit) {
        AddArray();
      } else {
        EditArray();
      }
      setOpen(false);
    } else {
      setOpenAlert(true);
    }
  };

  const AddArray = () => {
    let dataHandle = ArrayUser ? ArrayUser : [];
    let id = IdData;
    id++;

    dataHandle.push({ idData: id, name: nama, address: alamat, hobby: hobi });

    console.log(dataHandle);
    setArrayUser(dataHandle);
    setIdData(id);
    setSubmit(!Submit);
  };

  const EditArray = () => {
    let dataHandle = ArrayUser ? ArrayUser : [];
    const index = dataHandle.findIndex((obj) => {
      return obj.idData === Index;
    });
    dataHandle[index].name = nama;
    dataHandle[index].address = alamat;
    dataHandle[index].hobby = hobi;
    setArrayUser(dataHandle);
    setSubmit(!Submit);
  };

  return (
    <Router>
      <Header
        OpenModal={() => {
          setOpen(true);
          setNama("");
          setAlamat("");
          setHobi("");
          setIsEdit(false);
        }}
        SaveUser={() => setSave(false)}
        ArrayUser={ArrayUser}
      />
      <Switch>
        <Route exact path="/">
          <Form
            nama={nama}
            alamat={alamat}
            hobi={hobi}
            SetOpen={setOpen}
            SetNama={setNama}
            SetAlamat={setAlamat}
            SetHobi={setHobi}
            SetIsEdit={setIsEdit}
            open={open}
            ArrayUser={ArrayUser}
            SetIndex={setIndex}
            OpenAlert={OpenAlert}
            SetOpenAlert={setOpenAlert}
            validate={validate}
          />
        </Route>
        <Route exact path="/hobby">
          <Hobby
            open={open}
            SetOpen={setOpen}
            nama={nama}
            SetNama={setNama}
            alamat={alamat}
            SetAlamat={setAlamat}
            hobi={hobi}
            SetHobi={setHobi}
            validate={validate}
            OpenAlert={OpenAlert}
            SetOpenAlert={setOpenAlert}
          />
        </Route>
        <Route exact path="/about">
          <About 
            open={open}
            SetOpen={setOpen}
            nama={nama}
            SetNama={setNama}
            alamat={alamat}
            SetAlamat={setAlamat}
            hobi={hobi}
            SetHobi={setHobi}
            validate={validate}
            OpenAlert={OpenAlert}
            SetOpenAlert={setOpenAlert}
            />
        </Route>
        <Route exact path="/view/:id">
          <View 
          open={open}
          SetOpen={setOpen}
          nama={nama}
          SetNama={setNama}
          alamat={alamat}
          SetAlamat={setAlamat}
          hobi={hobi}
          SetHobi={setHobi}
          validate={validate}
          OpenAlert={OpenAlert}
          SetOpenAlert={setOpenAlert}
          />
        </Route>
      </Switch>
    </Router>
  );
};
