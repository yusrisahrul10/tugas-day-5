import {
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Grid,
  TextField,
  Typography,
  styled,
} from "@mui/material";
import React, { useState } from "react";

import { DialogUser } from "../assets/component/DialogUser";
import { green } from "@mui/material/colors";
import "../assets/css/form.css";
import { Alert } from "../assets/component/Alert";
import { NavLink, useHistory } from "react-router-dom";

export const Form = ({
  nama,
  alamat,
  hobi,
  SetOpen,
  SetNama,
  SetAlamat,
  SetHobi,
  SetIsEdit,
  open,
  ArrayUser,
  SetIndex,
  OpenAlert,
  SetOpenAlert,
  validate
}) => {
  const [InputSearch, setInpuSearch] = useState("");

  const history = useHistory();

  const ColorButton = styled(Button)(({ theme }) => ({
    color: theme.palette.getContrastText(green[700]),
    backgroundColor: green[700],
    "&:hover": {
      backgroundColor: green[990],
    },
    alignItems: "center",
  }));

  const handleSearch = () => {
    let dataHandle = ArrayUser ? ArrayUser : [];
    let inputHandler = (e) => {
      //convert input text to lower case
      var lowerCase = e.target.value.toLowerCase();
      setInpuSearch(lowerCase);
    };

    if (dataHandle.length !== 0) {
      return (
        <div className="search">
          <TextField
            id="outlined-basic"
            variant="outlined"
            fullWidth
            label="Search"
            onChange={inputHandler}
          />
        </div>
      );
    } else {
      return <div></div>;
    }
  };


  const handleBody = () => {
    let dataHandle = ArrayUser ? ArrayUser : [];
    const filteredData = dataHandle.filter((el) => {
      if (InputSearch === "") {
        return el;
      } else {
        return el.name.toLowerCase().includes(InputSearch);
      }
    });

    if (filteredData.length === 0) {
      return (
        <Container maxWidth="md" sx={{ position: "relative", zIndex: 1000 }}>
          <div className="imgCenter">
            <img src="https://external-preview.redd.it/9LjM8rKqaZ7mVwe5JbY9i7lRVb-qiMR_wCKo1UMBJ_I.jpg?auto=webp&s=4d70ec407d02dd0bcda5e8af46458a214e6d0aaf"></img>
          </div>
          <Typography
            variant="h2"
            component="h1"
            color="common.black"
            fontWeight="600"
            textAlign="center"
          >
            USER
          </Typography>
        </Container>
      );
    } else {
      return filteredData.map((value) => {
        return (
          <div key={value.idData}>
              <Card
            sx={{
              borderRadius: 7,
              marginTop: 3,
            }}
          >
            <CardContent>
              <Grid container sx={{ alignItems: "center" }}>
                <Grid item xs={10}>
                  <Typography
                    component="span"
                    sx={{
                      fontSize: 20,
                      fontWeight: 600,
                    }}
                  >
                    {value.name}
                  </Typography>
                  <Typography
                    sx={{
                      fontSize: 20,
                      color: "#909090",
                    }}
                  >
                    {value.address}
                  </Typography>
                </Grid>
                <Grid item xs={2} sx={{ textAlign: "center" }}>
                  <Typography
                    sx={{
                      fontSize: 20,
                      fontWeight: 540,
                    }}
                  >
                    {value.hobby}
                  </Typography>
                  <ColorButton
                    variant="contained"
                    sx={{ textTransform: "none", mr: 3 }}
                    onClick={() => history.push({
                      pathname: `/view/${value.idData}`,
                      state: { user: value}
                    })}
                  >
                    {/* <NavLink
                      to={{
                        
                      }}
                      underlined="none"
                      sx={{ textDecoration: "none" }}
                      className="button-link"
                    >
                      View
                    </NavLink> */}
                    View
                  </ColorButton>

                  <ColorButton
                    variant="contained"
                    sx={{ textTransform: "none" }}
                    onClick={() => {
                      SetOpen(true);
                      SetNama(value.name);
                      SetAlamat(value.address);
                      SetHobi(value.hobby);
                      SetIsEdit(true);
                      SetIndex(value.idData);
                    }}
                  >
                    Edit
                  </ColorButton>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
          </div>
        );
      });
    }
  };

  

  return (
    /* 1-12 
    xs HP sm IPAD md & xl laptop
    
    */
    <>
      <Box sx={{ flexGrow: 1 }}>
        <Grid container spacing={2} className="Layout">
          <Grid className="box1" item xs={12} sm={3} md={12} xl={12}></Grid>
          <Grid item xs={12} sm={3} md={12} xl={12} alignItems="center">
            {handleSearch()}
            {handleBody()}
          </Grid>
        </Grid>
      </Box>
      <Alert openAlert={OpenAlert} HandleClose={() => SetOpenAlert(false)} />
      <DialogUser
        opendialog={open}
        SetOpenDialog={SetOpen}
        nama = {nama}
        SetNamaUser = {SetNama}
        alamat = {alamat}
        SetAlamatUser = {SetAlamat}
        hobi = {hobi}
        SetHobiUser = {SetHobi}
        validate={validate}
      />
    </>
  );
};
