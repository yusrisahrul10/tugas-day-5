import { Card, CardContent, Grid, Typography } from "@mui/material";
import React from "react";
import { useLocation } from "react-router-dom";
import { DialogUser } from "../assets/component/DialogUser";
import { Alert } from "../assets/component/Alert";

export const View = ({
  open,
  SetOpen,
  nama,
  SetNama,
  alamat,
  SetAlamat,
  hobi,
  SetHobi,
  validate,
  OpenAlert,
  SetOpenAlert,
}) => {
  const location = useLocation();
  const myUser = location.state.user;

  return (
    <>
      <Alert openAlert={OpenAlert} HandleClose={() => SetOpenAlert(false)} />
      <DialogUser
        opendialog={open}
        SetOpenDialog={SetOpen}
        nama={nama}
        SetNamaUser={SetNama}
        alamat={alamat}
        SetAlamatUser={SetAlamat}
        hobi={hobi}
        SetHobiUser={SetHobi}
        validate={validate}
      />
      <Card
        sx={{
          borderRadius: 7,
          marginTop: 3,
        }}
      >
        <CardContent>
          <Grid container sx={{ alignItems: "center" }}>
            <Grid item xs={10}>
              <Typography
                component="span"
                sx={{
                  fontSize: 20,
                  fontWeight: 600,
                }}
              >
                {myUser.name}
              </Typography>
              <Typography
                sx={{
                  fontSize: 20,
                  color: "#909090",
                }}
              >
                {myUser.address}
              </Typography>
            </Grid>
            <Grid item xs={2} sx={{ textAlign: "center" }}>
              <Typography
                sx={{
                  fontSize: 20,
                  fontWeight: 540,
                }}
              >
                {myUser.hobby}
              </Typography>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </>
  );
};
