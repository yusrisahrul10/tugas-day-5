import { Typography } from "@mui/material";
import React from "react";
import { DialogUser } from "../assets/component/DialogUser";
import { Alert } from "../assets/component/Alert";

export const About = ({
  open,
  SetOpen,
  nama,
  SetNama,
  alamat,
  SetAlamat,
  hobi,
  SetHobi,
  validate,
  OpenAlert,
  SetOpenAlert,
}) => {
  return (
    <>
      <Typography variant="body2" color="text.secondary" align="center">
        {"MyApp Copyright © Yusri Sahrul "}
        {new Date().getFullYear()}
        {"."}
      </Typography>
      <Alert openAlert={OpenAlert} HandleClose={() => SetOpenAlert(false)} />
      <DialogUser
        opendialog={open}
        SetOpenDialog={SetOpen}
        nama={nama}
        SetNamaUser={SetNama}
        alamat={alamat}
        SetAlamatUser={SetAlamat}
        hobi={hobi}
        SetHobiUser={SetHobi}
        validate={validate}
      />
    </>
  );
};
