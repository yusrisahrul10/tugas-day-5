import { Container, Typography } from "@mui/material";
import React from "react";
import { useLocation } from "react-router-dom";
import { DialogUser } from "../assets/component/DialogUser";
import { Alert } from "../assets/component/Alert";

export const Hobby = ({
  open,
  SetOpen,
  nama,
  SetNama,
  alamat,
  SetAlamat,
  hobi,
  SetHobi,
  validate,
  OpenAlert,
  SetOpenAlert,
}) => {
  const location = useLocation();
  const myArray = location.state.myArray;

  console.log(myArray);
  if (myArray.length === 0) {
    return (
      <>
        <Container maxWidth="md" sx={{ position: "relative", zIndex: 1000 }}>
          <div className="imgCenter">
            <img src="https://external-preview.redd.it/9LjM8rKqaZ7mVwe5JbY9i7lRVb-qiMR_wCKo1UMBJ_I.jpg?auto=webp&s=4d70ec407d02dd0bcda5e8af46458a214e6d0aaf"></img>
          </div>
          <Typography
            variant="h2"
            component="h1"
            color="common.black"
            fontWeight="600"
            textAlign="center"
          >
            USER
          </Typography>
        </Container>
        <Alert openAlert={OpenAlert} HandleClose={() => SetOpenAlert(false)} />
        <DialogUser
          opendialog={open}
          SetOpenDialog={SetOpen}
          nama={nama}
          SetNamaUser={SetNama}
          alamat={alamat}
          SetAlamatUser={SetAlamat}
          hobi={hobi}
          SetHobiUser={SetHobi}
          validate={validate}
        />
      </>
    );
  } else {
    return (
      <>
        <Alert openAlert={OpenAlert} HandleClose={() => SetOpenAlert(false)} />
        <DialogUser
          opendialog={open}
          SetOpenDialog={SetOpen}
          nama={nama}
          SetNamaUser={SetNama}
          alamat={alamat}
          SetAlamatUser={SetAlamat}
          hobi={hobi}
          SetHobiUser={SetHobi}
          validate={validate}
        />
        <ul>
          {myArray.map((item) => (
            <li key={item.idData}>{item.hobby}</li>
          ))}
        </ul>
      </>
    );
  }
};
